package com.benjidea.wikitudejavascriptdemo;

import android.content.Context;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.wikitude.architect.ArchitectView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ARFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ARFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ARFragment extends AbstractArchitectCamFragmentV4 {

    private OnFragmentInteractionListener mListener;

    public ARFragment() {
        // Required empty public constructor
    }


    public static ARFragment newInstance() {
        ARFragment fragment = new ARFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ar, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        ((ImageButton) view.findViewById(R.id.cameraButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteraction();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public String getARchitectWorldPath() {
        return "";
    }

    @Override
    public ArchitectView.ArchitectUrlListener getUrlListener() {
        return null;
    }

    @Override
    public int getContentViewId() {
        return R.layout.fragment_ar;
    }

    @Override
    public String getWikitudeSDKLicenseKey() {
        return "";
    }

    @Override
    public int getArchitectViewId() {
        return R.id.architectView;
    }

    @Override
    public ILocationProvider getLocationProvider(LocationListener locationListener) {
        return null;
    }

    @Override
    public ArchitectView.SensorAccuracyChangeListener getSensorAccuracyListener() {
        return null;
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction();
    }
}
