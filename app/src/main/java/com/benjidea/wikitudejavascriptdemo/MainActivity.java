package com.benjidea.wikitudejavascriptdemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements ARFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, ARFragment.newInstance(), "AR")
                .commit();
    }

    @Override
    public void onFragmentInteraction() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, ContentFragment.newInstance(), "Content")
                .addToBackStack("debug")
                .commit();
    }
}
